var mainMenuToggleButton = document.getElementsByClassName('nav-toggle')[0],
    mainMenu = document.getElementsByClassName('nav-menu')[0];

mainMenuToggleButton.addEventListener('click', toggleMainMenuHandler);

function toggleMainMenuHandler() {
    toggleActive(mainMenuToggleButton);
    toggleActive(mainMenu);
}