var modalToggleButtons = document.getElementsByClassName('modal-toggle'),
    modalBackgrounds = document.getElementsByClassName('modal-background'),
    modalCloseButtons = document.getElementsByClassName('modal-close-button'),
    modalCancelButtons = document.getElementsByClassName('modal-cancel-button');

for (var button of modalToggleButtons)
{
    button.addEventListener('click', toggleModalHandler);
}

for (var background of modalBackgrounds)
{
    background.addEventListener('click', clickedModalBackgroundHandler);
}

for (var button of modalCloseButtons)
{
    button.addEventListener('click', closeModalHandler);
}

for (var button of modalCancelButtons)
{
    button.addEventListener('click', closeModalHandler);
}

function closeModalHandler()
{
    var modal = getClosest(this, '.modal');
    toggleActive(modal);
}

function clickedModalBackgroundHandler()
{
    toggleActive(this.parentElement);
}

function toggleModalHandler(event)
{
    event.preventDefault();

    var modal = document.getElementById(this.getAttribute('data-modal'));
    toggleActive(modal);
}

function toggleActive(element)
{
    element.classList.toggle('is-active');
}