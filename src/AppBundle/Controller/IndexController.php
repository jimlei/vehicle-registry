<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;

class IndexController
{
    private $request;
    private $templateEngine;

    public function __construct(
        Request $request,
        EngineInterface $templateEngine)
    {
        $this->request = $request;
        $this->templateEngine = $templateEngine;
    }

    public function getAction()
    {
        return $this->templateEngine->renderResponse('default/index.html.twig', [
        ]);
    }
}