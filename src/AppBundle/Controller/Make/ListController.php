<?php

namespace AppBundle\Controller\Make;

use AppBundle\Repository\MakeRepository;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;

class ListController
{
    private $repo;
    private $request;
    private $templateEngine;

    public function __construct(
        Request $request,
        EngineInterface $templateEngine,
        MakeRepository $repo)
    {
        $this->request = $request;
        $this->templateEngine = $templateEngine;
        $this->repo = $repo;
    }

    public function getAction()
    {
        return $this->templateEngine->renderResponse('default/make/list.html.twig', [
            'makes' => $this->repo->findAllThatHasVehicles()
        ]);
    }
}