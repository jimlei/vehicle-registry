<?php

namespace AppBundle\Controller\Make;

use AppBundle\Repository\MakeRepository;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;

class SingleController
{
    private $repo;
    private $request;
    private $templateEngine;

    public function __construct(
        Request $request,
        EngineInterface $templateEngine,
        MakeRepository $repo)
    {
        $this->request = $request;
        $this->templateEngine = $templateEngine;
        $this->repo = $repo;
    }

    public function getAction($slug)
    {
        try
        {
            $make = $this->repo->findOneBySlug($slug);

            return $this->templateEngine->renderResponse('default/make/single.html.twig', [
                'make' => $make
            ]);
        }
        catch(ORMException $e)
        {
            throw new NotFoundHttpException();
        }
    }
}