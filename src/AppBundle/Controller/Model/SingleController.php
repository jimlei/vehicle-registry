<?php

namespace AppBundle\Controller\Model;

use AppBundle\Repository\ModelRepository;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class SingleController
{
    private $repo;
    private $templateEngine;

    public function __construct(
        EngineInterface $templateEngine,
        ModelRepository $repo)
    {
        $this->templateEngine = $templateEngine;
        $this->repo = $repo;
    }

    public function getAction($slug)
    {
        try
        {
            $model = $this->repo->findOneBySlug($slug);

            return $this->templateEngine->renderResponse('default/model.html.twig', [
                'model' => $model
            ]);
        }
        catch(ORMException $e)
        {
            throw new NotFoundHttpException();
        }
    }
}