<?php

namespace AppBundle\Controller\Vehicle;

use AppBundle\Entity\Vehicle;
use AppBundle\Form\VehicleType;
use AppBundle\Repository\ModelRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

class CreateController
{
    private $manager;
    private $formFactory;
    private $repo;
    private $request;
    private $router;
    private $templateEngine;

    public function __construct(
        Request $request,
        RouterInterface $router,
        EntityManagerInterface $manager,
        EngineInterface $templateEngine,
        FormFactoryInterface $formFactory,
        ModelRepository $repo)
    {
        $this->manager = $manager;
        $this->request = $request;
        $this->router = $router;
        $this->templateEngine = $templateEngine;
        $this->formFactory = $formFactory;
        $this->repo = $repo;
    }

    public function getAction($modelSlug)
    {
        try
        {
            $model = $this->repo->findOneBySlug($modelSlug);

            $vehicle = new Vehicle();
            $vehicle->setModel($model);
            $vehicleForm = $this->formFactory->create(VehicleType::class, $vehicle, [
                'showModel' => false
            ]);

            $vehicleForm->handleRequest($this->request);
            if ($vehicleForm->isSubmitted() && $vehicleForm->isValid()) {
                $this->manager->persist($vehicle);
                $this->manager->flush();
                return new RedirectResponse($this->router->generate('vehicle_single', [
                    'modelSlug' => $vehicle->getModel()->getSlug(),
                    'vin' => $vehicle->getVin()
                ]));
            }

            return $this->templateEngine->renderResponse('default/vehicle/create.html.twig', [
                'model' => $model,
                'form' => $vehicleForm->createView()
            ]);
        }
        catch(ORMException $e)
        {
            throw new NotFoundHttpException();
        }
    }
}