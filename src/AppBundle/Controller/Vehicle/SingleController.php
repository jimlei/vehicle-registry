<?php

namespace AppBundle\Controller\Vehicle;

use AppBundle\Form\VehicleSpesificationType;
use AppBundle\Repository\VehicleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

class SingleController
{
    private $manager;
    private $formFactory;
    private $repo;
    private $request;
    private $router;
    private $templateEngine;

    public function __construct(
        Request $request,
        RouterInterface $router,
        EntityManagerInterface $manager,
        EngineInterface $templateEngine,
        FormFactoryInterface $formFactory,
        VehicleRepository $repo)
    {
        $this->manager = $manager;
        $this->request = $request;
        $this->router = $router;
        $this->templateEngine = $templateEngine;
        $this->formFactory = $formFactory;
        $this->repo = $repo;
    }

    public function getAction($vin)
    {
        try
        {
            $vehicle = $this->repo->findOneByVin($vin);

            $spesificationForm = $this->formFactory->create(VehicleSpesificationType::class, $vehicle);

            $spesificationForm->handleRequest($this->request);

            if ($spesificationForm->isSubmitted() && $spesificationForm->isValid()) {

                $this->manager->persist($vehicle);
                $this->manager->flush();

                return new RedirectResponse($this->router->generate('vehicle_single', [
                    'modelSlug' => $vehicle->getModel()->getSlug(),
                    'vin' => $vin
                ]));
            }

            return $this->templateEngine->renderResponse('default/vehicle/single.html.twig', [
                'vehicle' => $vehicle,
                'spesificationForm' => $spesificationForm->createView()
            ]);
        }
        catch(ORMException $e)
        {
            throw new NotFoundHttpException();
        }
    }
}