<?php

namespace AppBundle\Listener;

use AppBundle\Repository\VehicleRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Oneup\UploaderBundle\Event\PostPersistEvent;

class UploadListener
{
    /**
     * @var ObjectManager
     */
    private $om;
    private $vehicleRepository;

    public function __construct(ObjectManager $om, VehicleRepository $vehicleRepository)
    {
        $this->om = $om;
        $this->vehicleRepository = $vehicleRepository;
    }

    public function onUpload(PostPersistEvent $event)
    {
        switch($event->getType())
        {
            case 'gallery':
                $this->galleryImageUploaded($event);
                return $this->renderResponse($event);
        }

        return $this->renderResponse($event, false);
    }

    private function galleryImageUploaded(PostPersistEvent $event)
    {
        $request = $event->getRequest();
        $vehicle = $this->vehicleRepository->find($request->get('vehicleId'));
    }

    private function renderResponse(PostPersistEvent $event, $success = true)
    {
        $response = $event->getResponse();
        $response['success'] = $success;
        return $response;
    }
}