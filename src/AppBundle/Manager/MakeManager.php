<?php

namespace AppBundle\Manager;


use AppBundle\Entity\Make;
use AppBundle\Repository\MakeRepository;
use Doctrine\ORM\EntityManagerInterface;

class MakeManager
{
    private $repo;

    public function __construct(EntityManagerInterface $doctrine, MakeRepository $repo)
    {
        $this->doctrine = $doctrine;
        $this->repo = $repo;
    }

    public function save(Make $make)
    {
        $this->doctrine->persist($make);
        $this->doctrine->flush();
    }
}