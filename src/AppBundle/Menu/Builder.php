<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class Builder
{
    private $factory;
    private $user;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(TokenStorageInterface $tokenStorage, FactoryInterface $factory)
    {
        $this->factory = $factory;
        $this->user = $tokenStorage->getToken()->getUser();
    }

    public function createMainMenu(array $options)
    {
        $menu = $this->getMainMenuItems();

        foreach($menu->getChildren() as $child)
        {
            $child->setAttribute('class', 'is-hidden-mobile');
        }

        return $menu;
    }

    public function createMobileMainMenu(array $options)
    {
        $menu = $this->getMainMenuItems();

        foreach($menu->getChildren() as $child)
        {
            $child->setAttribute('class', 'is-hidden-tablet');
        }

        return $menu;
    }

    private function getMainMenuItems()
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Home', array('route' => 'index'));
        $menu->addChild('Registry', array('route' => 'make_list'));

        return $menu;
    }

    public function createUserMenu(array $options)
    {
        $menu = $this->factory->createItem('root');

        if ($this->user === 'anon.')
        {
            $menu->addChild('Log in', array('route' => 'fos_user_security_login'));
        }
        else
        {
            $menu->addChild('Profile', array('route' => 'fos_user_profile_show'))->setAttribute('icon', 'fa-user-circle-o');
            $menu->addChild('Log out', array('route' => 'fos_user_security_logout'));
        }

        return $menu;
    }
}