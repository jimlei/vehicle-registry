<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Model;

/**
 * ModelRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ModelRepository extends EntityRepository
{
    public function findOneBySlug($slug) : Model
    {
        $qb = $this->createQueryBuilder('model');

        $qb->addSelect('make, vehicles')
            ->join('model.make', 'make')
            ->join('model.vehicles', 'vehicles')
            ->where($qb->expr()->eq('model.slug', '?0'))
            ->orderBy('vehicles.vin')
            ->setParameters([$slug]);

        return $qb->getQuery()->getOneOrNullResult();
    }
}