<?php

namespace AppBundle\Security;

use AppBundle\Entity\Model;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ModelVoter extends Voter
{
    // these strings are just invented: you can use anything
    const VIEW = 'view';
    const EDIT = 'edit';
    const ADD_VEHICLE = 'addVehicle';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::VIEW, self::EDIT, self::ADD_VEHICLE))) {
            return false;
        }

        if (!$subject instanceof Model) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var Model $model */
        $model = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($model, $user);
            case self::EDIT:
                return $this->canEdit($model, $user);
            case self::ADD_VEHICLE:
                return $this->canAddVehicle($model, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canAddVehicle(Model $model, User $user)
    {
        return true;
    }

    private function canView(Model $model, User $user)
    {
        return true;
    }

    private function canEdit(Model $model, User $user)
    {
        return true;
    }
}